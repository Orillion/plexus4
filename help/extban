Extbans are split into two types: matching extbans, which match on
users in different ways, and acting extbans, which restrict users
in different ways to a standard ban.

To use an extban, simply set +b/e/I <ban> with it as the ban,
instead of a normal nick!user@host mask, to ban or exempt matching
users.

Matching extbans:

 a:<account>   Matches users logged into a matching account.

 f:<channel>   Allows redirect bans from the matching channel.
               May only be set on the exception list.

 r:<realname>  Matches users with a matching realname.

 z:<certfp>    Matches users having the given SSL certificate
               fingerprint.

Acting extbans:

 m:<banmask>   Blocks messages from matching users. Users with voice
               or above are not affected.

A ban given to an acting extban may either be a nick!user@host mask,
matched against users as a normal ban, or a matching extban.

There is an additional special type of extended ban, a redirect ban:

 Redirect      n!u@h#channel will redirect the banned user to #channel
               when they try to join.

Redirect bans require the target channel to allow the redirect with extban f.

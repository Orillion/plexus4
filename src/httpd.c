/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "httpd.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "send.h"
#include "snomask.h"

#ifdef HAVE_LIBMICROHTTPD

#include <microhttpd.h>

static struct MHD_Daemon *httpd;
static fde_t httpd_efd; // epoll fd of mhd

static dlink_list resources;

static int
httpd_handle_request(void *cls, struct MHD_Connection *connection, const char *url,
                     const char *method, const char *version, const char *upload_data,
                     size_t *upload_data_size, void **con_cls)
{
  dlink_node *node;
  char buf[IRCD_BUFSIZE];

  if (!method || !url)
    return MHD_NO;

  const union MHD_ConnectionInfo *info = MHD_get_connection_info(connection, MHD_CONNECTION_INFO_CLIENT_ADDRESS);
  struct sockaddr *addr = info->client_addr;
  int len = addr->sa_family == AF_INET6 ? sizeof(struct sockaddr_in6) : sizeof(struct sockaddr_in);
  getnameinfo(addr, len, buf, sizeof(buf), NULL, 0, NI_NUMERICHOST);

  sendto_snomask(SNO_DEBUG, L_ALL, "HTTP %s %s from %s", method, url, buf);

  DLINK_FOREACH(node, resources.head)
  {
    struct HttpResource *resource = node->data;

    if (resource->url && !strcmp(resource->url, url)
        && resource->method && !strcmp(resource->method, method)
        && resource->handler)
      return resource->handler(connection, url, method, version, upload_data, upload_data_size);
  }

  return MHD_NO;
}

static void
httpd_ready(fde_t *fd, void *data)
{
  MHD_run(httpd);

  comm_setselect(&httpd_efd, COMM_SELECT_READ, httpd_ready, NULL);
}

void
httpd_start()
{
  httpd_stop();

  httpd = MHD_start_daemon(MHD_USE_EPOLL_LINUX_ONLY, 0, NULL, NULL, httpd_handle_request, NULL, MHD_OPTION_END);

  if (httpd == NULL)
    return;

  /* for some reason can't use both MHD_USE_EPOLL_LINUX_ONLY and MHD_USE_NO_LISTEN_SOCKET, so close
   * its listening socket now
   */
  int *fd = (int *) MHD_get_daemon_info(httpd, MHD_DAEMON_INFO_LISTEN_FD);
  if (fd)
  {
    close(*fd);
    *fd = -1;
  }

  const union MHD_DaemonInfo *info = MHD_get_daemon_info(httpd, MHD_DAEMON_INFO_EPOLL_FD_LINUX_ONLY); // the epoll fd
  if (info == NULL)
  {
    httpd_stop();
    return;
  }

  int epoll_fd = * (const int *) info;
  fd_open(&httpd_efd, epoll_fd, 1, "microhttpd epoll fd");

  comm_setselect(&httpd_efd, COMM_SELECT_READ, httpd_ready, NULL);
}

void
httpd_stop()
{
  if (httpd != NULL)
  {
    int *efd = (int*) MHD_get_daemon_info(httpd, MHD_DAEMON_INFO_EPOLL_FD_LINUX_ONLY);
    if (efd)
    {
      *efd = -1;
    }

    fd_close(&httpd_efd);

    MHD_stop_daemon(httpd);
    httpd = NULL;
  }
}

void
httpd_register(struct HttpResource *resource)
{
  if (resource->method == NULL || resource->url == NULL || resource->handler == NULL)
    return;

  dlinkAdd(resource, &resource->node, &resources);
}

void
httpd_unregister(struct HttpResource *resource)
{
  if (resource->method == NULL || resource->url == NULL || resource->handler == NULL)
    return;

  dlinkDelete(&resource->node, &resources);
}

int
httpd_add_connection(int fd, struct irc_ssaddr *addr)
{
  if (httpd == NULL)
    return MHD_NO;

  return MHD_add_connection(httpd, fd, (struct sockaddr *) addr, addr->ss_len) == MHD_YES;
}
#else

void
httpd_start()
{
}

void
httpd_stop()
{
}

int
httpd_add_connection(int fd, struct irc_ssaddr *addr)
{
  return 0;
}

#endif


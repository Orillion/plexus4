function iline_attach(iline_attach_data)
  local client = iline_attach_data.client
  local conf = iline_attach_data.mask

  if conf.name ~= 'tor' or not client.certfp then
    return
  end

  local host = client.certfp .. ".tor.rizon.net"
  -- place in fd::/8
  local ip = "FD"
  -- certfp is base16, 40 characters
  for i = 3,32 do
    ip = ip .. client.certfp:sub(i, i)
    if math.fmod(i, 4) == 0 and i ~= 32 then
      ip = ip .. ':'
    end
  end

  client.host = host
  client.realhost = host
  client.localClient.cgisockhost = client.sockhost
  client.sockhost = ip
  plexus.client_set_ip(client, ip)

  -- unset IP spoof flag set from the name being 'tor' so the proper IP is displayed
  plexus.client_unset_flag(client, plexus.FLAGS_IP_SPOOFING)

  iline_attach_data.check_klines = 1
end

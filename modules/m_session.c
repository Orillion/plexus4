/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"
#include "memory.h"
#include "hostmask.h"
#include "irc_string.h"
#include "send.h"
#include "s_misc.h"

/* SESSION mask limit creator created expires reason */
static void
me_session(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask = parv[1],
             *limitstr = parv[2],
             *creator = parv[3],
             *createdstr = parv[4],
             *expiresstr = parv[5],
             *reason = parv[parc - 1];

  struct Client *server = IsServer(source_p) ? source_p : source_p->servptr;

  if (!find_matching_name_conf(CONF_ULINE, server->name,
                               NULL, NULL,
                               SHARED_SESSION))
    return;

  struct irc_ssaddr addr;
  int bits;

  int masktype = parse_netmask(mask, &addr, &bits);

  struct MaskLookup lookup = {
    .name = mask,
    .fam = masktype != HM_HOST ? addr.ss.ss_family : 0,
    .addr = masktype != HM_HOST ? &addr : NULL,
    .cmpfunc = irccmp
  };

  struct MaskItem *session = find_conf_by_address(CONF_SESSION, &lookup);

  if (session != NULL)
  {
    delete_one_address_conf(session->host, session);
  }

  session = conf_make(CONF_SESSION);

  session->count = atol(limitstr);
  session->user = xstrdup(creator);
  session->host = xstrdup(mask);
  session->setat = atol(createdstr);
  session->until = atol(expiresstr);
  session->reason = xstrdup(reason);

  SetConfDatabase(session);

  add_conf_by_address(CONF_SESSION, session);

  sendto_snomask(SNO_DEBUG, L_ALL,
                 "%s added session exception for %s, limit %d, to expire on %s",
                 source_p->name, session->host, session->count, session->until != 0 ? myctime(session->until) : "never");
}

/* UNSESSION mask */
static void
me_unsession(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask = parv[1];

  struct Client *server = IsServer(source_p) ? source_p : source_p->servptr;

  if (!find_matching_name_conf(CONF_ULINE, server->name,
                               NULL, NULL,
                               SHARED_SESSION))
    return;

  struct irc_ssaddr addr;
  int bits;

  int masktype = parse_netmask(mask, &addr, &bits);

  struct MaskLookup lookup = {
    .name = mask,
    .fam = masktype != HM_HOST ? addr.ss.ss_family : 0,
    .addr = masktype != HM_HOST ? &addr : NULL,
    .cmpfunc = irccmp
  };

  struct MaskItem *session = find_conf_by_address(CONF_SESSION, &lookup);

  if (session == NULL)
    return;

  sendto_snomask(SNO_DEBUG, L_ALL,
                 "%s removed session exception for %s",
                 source_p->name, session->host);

  delete_one_address_conf(session->host, session);
}

static struct Message session_msgtab = {
  .cmd = "SESSION",
  .args_min = 6,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_ignore,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_session,
  .handlers[OPER_HANDLER] = m_ignore,
};

static struct Message unsession_msgtab = {
  .cmd = "UNSESSION",
  .args_min = 2,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_ignore,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_unsession,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&session_msgtab);
  mod_add_cmd(&unsession_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&session_msgtab);
  mod_del_cmd(&unsession_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};


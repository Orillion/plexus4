/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file parse.h
 * \brief A header for the message parser.
 * \version $Id$
 */

#ifndef INCLUDED_parse_h
#define INCLUDED_parse_h

struct Client;
struct Message;


/*
 * m_functions execute protocol messages on this server:
 * void m_func(struct Client* client_p, struct Client* source_p, int parc, char* parv[]);
 *
 *    client_p    is always NON-NULL, pointing to a *LOCAL* client
 *            structure (with an open socket connected!). This
 *            identifies the physical socket where the message
 *            originated (or which caused the m_function to be
 *            executed--some m_functions may call others...).
 *
 *    source_p    is the source of the message, defined by the
 *            prefix part of the message if present. If not
 *            or prefix not found, then source_p==client_p.
 *
 *            (!IsServer(client_p)) => (client_p == source_p), because
 *            prefixes are taken *only* from servers...
 *
 *            (IsServer(client_p))
 *                    (source_p == client_p) => the message didn't
 *                    have the prefix.
 *
 *                    (source_p != client_p && IsServer(source_p) means
 *                    the prefix specified servername. (?)
 *
 *                    (source_p != client_p && !IsServer(source_p) means
 *                    that message originated from a remote
 *                    user (not local).
 *
 *
 *            combining
 *
 *            (!IsServer(source_p)) means that, source_p can safely
 *            taken as defining the target structure of the
 *            message in this server.
 *
 *    *Always* true (if 'parse' and others are working correct):
 *
 *    1)      source_p->from == client_p  (note: client_p->from == client_p)
 *
 *    2)      MyConnect(source_p) <=> source_p == client_p (e.g. source_p
 *            *cannot* be a local connection, unless it's
 *            actually client_p!). [MyConnect(x) should probably
 *            be defined as (x == x->from) --msa ]
 *
 *    parc    number of variable parameter strings (if zero,
 *            parv is allowed to be NULL)
 *
 *    parv    a NULL terminated list of parameter pointers,
 *
 *                    parv[0], sender (prefix string), if not present
 *                            this points to an empty string.
 *                    parv[1]...parv[parc-1]
 *                            pointers to additional parameters
 *                    parv[parc] == NULL, *always*
 *
 *            note:   it is guaranteed that parv[0]..parv[parc-1] are all
 *                    non-NULL pointers.
 */

/*
 * MessageHandler
 */
typedef enum HandlerType {
  UNREGISTERED_HANDLER,
  CLIENT_HANDLER,
  SERVER_HANDLER,
  ENCAP_HANDLER,
  OPER_HANDLER,
  LAST_HANDLER_TYPE
} HandlerType;

/*
 * MessageHandler function
 * Params:
 * struct Client* client_p   - connection message originated from
 * struct Client* source_p   - source of message, may be different from client_p
 * int            parc   - parameter count
 * char*          parv[] - parameter vector
 */
typedef void (*MessageHandler)(struct Client *, struct Client *, int, char *[]);

typedef void (*MessageHandlerEx)(struct Message *, struct Client *, int, char *[]);

/*
 * Message table structure
 */
struct Message
{
  const char *cmd;
  unsigned int count;      /* number of times command used */
  unsigned int rcount;     /* number of times command used by server */
  unsigned int args_min; /* at least this many args must be passed
                             * or an error will be sent to the user
                             * before the m_func is even called
                             */
  unsigned int args_max;    /* maximum permitted parameters */
  unsigned int flags;
  uint64_t bytes;  /* bytes received for this message */

  /*
   * client_p = Connected client ptr
   * source_p = Source client ptr
   * parc = parameter count
   * parv = parameter variable array
   */
  /* handlers:
   * UNREGISTERED, CLIENT, SERVER, ENCAP, OPER, LAST
   */
  MessageHandler handlers[LAST_HANDLER_TYPE];
};

/*
 * Constants
 */
enum
{
  /*
   * if this is specified, use a different proto for the handler
   * (MessageHandlerEx).
   */
  MFLG_EX_DISPATCH = 0x1,
  MFLG_UNKNOWN = 0x2,
  MFLG_CAPTURED = 0x4 /* allow when full captured */
};

#define MAXPARA    15

extern void parse(struct Client *, char *, char *);
extern void mod_add_cmd(struct Message *);
extern void mod_del_cmd(struct Message *);
extern struct Message *find_command(const char *);
extern void report_messages(struct Client *);

/* generic handlers */
extern void m_ignore(struct Client *, struct Client *, int, char *[]);
extern void m_not_oper(struct Client *, struct Client *, int, char *[]);
extern void m_registered(struct Client *, struct Client *, int, char *[]);
extern void m_unregistered(struct Client *, struct Client *, int, char *[]);

/*
 * (based on orabidoo's parser code)
 *
 * This has always just been a trie. Look at volume III of Knuth ACP
 *
 *
 * ok, you start out with an array of pointers, each one corresponds
 * to a letter at the current position in the command being examined.
 *
 * so roughly you have this for matching 'trie' or 'tie'
 *
 * 't' points -> [MessageTree *] 'r' -> [MessageTree *] -> 'i'
 *   -> [MessageTree *] -> [MessageTree *] -> 'e' and matches
 *
 *                               'i' -> [MessageTree *] -> 'e' and matches
 *
 * BUGS (Limitations!)
 *
 * I designed this trie to parse ircd commands. Hence it currently
 * casefolds. This is trivial to fix by increasing MAXPTRLEN.
 * This trie also "folds" '{' etc. down. This means, the input to this
 * trie must be alpha tokens only. This again, is a limitation that
 * can be overcome by increasing MAXPTRLEN to include upper/lower case
 * at the expense of more memory. At the extreme end, you could make
 * MAXPTRLEN 128.
 *
 * This is also not a patricia trie. On short ircd tokens, this is
 * not likely going to matter.
 *
 * Diane Bruce (Dianora), June 6 2003
 */

/* Must be a power of 2, and
 * larger than 26 [a-z]|[A-Z]
 * its used to allocate the set
 * of pointers at each node of the tree
 * There are MAXPTRLEN pointers at each node.
 * Obviously, there have to be more pointers
 * Than ASCII letters. 32 is a nice number
 * since there is then no need to shift
 * 'A'/'a' to base 0 index, at the expense
 * of a few never used pointers. For a small
 * parser like this, this is a good compromise
 * and does make it somewhat faster.
 *
 * - Dianora
 */
#define MAXPTRLEN 32

struct MessageTree
{
  int links; /* Count of all pointers (including msg) at this node
              * used as reference count for deletion of _this_ node.
              */
  struct Message *msg;
  struct MessageTree *pointers[MAXPTRLEN];
};

extern struct MessageTree msg_tree;


#endif /* INCLUDED_parse_h */

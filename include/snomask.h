/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

struct Snomask
{
  char mode;
  int remote; /* whether or not remote version is available, too */
  unsigned int flag;
  dlink_node node;
};

struct Snomasks
{
  unsigned int local;
  unsigned int remote;
};

enum
{
  /* send to the remote version of the snomask */
  SNO_REMOTE   = 4,
  /* route snomask remotely */
  SNO_ROUTE    = 8,

  /* all possible mask values */
  SNOMASK_MASK = 0xFFFFFFFF
};

extern struct Snomask sno_cconn,
                      sno_rej,
                      sno_skill,
                      sno_full,
                      sno_spy,
                      sno_debug,
                      sno_nchange,
                      sno_bots,
                      sno_link,
                      sno_unauth;

#define SNO_CCONN          (&sno_cconn)
#define SNO_REJ            (&sno_rej)
#define SNO_SKILL          (&sno_skill)
#define SNO_FULL           (&sno_full)
#define SNO_SPY            (&sno_spy)
#define SNO_DEBUG          (&sno_debug)
#define SNO_NCHANGE        (&sno_nchange)
#define SNO_BOTS           (&sno_bots)
#define SNO_LINK           (&sno_link)
#define SNO_UNAUTH         (&sno_unauth)
#define SNO_ALL            NULL

extern void snomask_init(void);
extern void snomask_add(struct Snomask *);
extern void snomask_del(struct Snomask *);
extern struct Snomask *snomask_find(char);
extern const char *snomask_to_str(struct Snomasks *);
extern void snomask_parse(const char *, struct Snomasks *, struct Snomasks *);
extern void snomask_set(struct Snomasks *, struct Snomasks *, struct Snomasks *);
extern int snomask_has(struct Snomasks *, struct Snomask *, int);

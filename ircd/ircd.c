#include "config.h"
#include <stdio.h>
#include <dlfcn.h>
#include <setjmp.h>
#include <syslog.h>

#include <openssl/ssl.h>

#ifdef HAVE_LIBJANSSON
#include <jansson.h>

json_t *plexus_upgrade = NULL;
#endif

jmp_buf plexus_upgrade_jmp;

extern void ircd_free_all();

static void
ilog(int type, const char *pattern, ...)
{
  char buffer[512];
  va_list args;

  va_start(args, pattern);
  vsnprintf(buffer, sizeof(buffer), pattern, args);
  va_end(args);

  openlog("ircd", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
  syslog(type, "%s", buffer);
  closelog();
}

int main(int argc, char **argv)
{
  SSL_library_init();
#ifdef HAVE_LIBJANSSON
  json_decref(json_object());
#endif

  for (;;)
  {
    union
    {
      int (*f)(int, char **);
      void *ptr;
    } u;

    ircd_free_all();

    void *handle = dlopen(LIBDIR "/" PACKAGE "/libplexus.so", RTLD_NOW | RTLD_GLOBAL);
    if (!handle)
    {
      ilog(LOG_ERR, "Unable to open libplexus: %s", dlerror());
      return -1;
    }

    ilog(LOG_INFO, "Loaded libplexus at %p", handle);

    u.ptr = dlsym(handle, "plexus_main");
    if (!u.ptr)
    {
      ilog(LOG_ERR, "Unable to find plexus_main: %s", dlerror());
      dlclose(handle);
      return -1;
    }

    if (!setjmp(plexus_upgrade_jmp))
      u.f(argc, argv);

    if (dlclose(handle))
    {
      ilog(LOG_ERR, "Unable to close libplexus: %s", dlerror());
      return -1; /* probably not safe to rerun plexus_main() here because static variables will not be reinitialized */
    }

    handle = dlopen(LIBDIR "/" PACKAGE "/libplexus.so", RTLD_NOW | RTLD_GLOBAL | RTLD_NOLOAD);
    if (handle)
    {
      ilog(LOG_ERR, "libplexus is still loaded in memory after dlclose() at %p", handle);
      dlclose(handle);
      return -1;
    }

    ilog(LOG_INFO, "Unloaded libplexus");
  }
}

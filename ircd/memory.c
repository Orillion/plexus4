#include <stdlib.h>
#include <stddef.h>

struct chunk
{
  struct chunk *prev, *next;
  char memory[];
};

static struct chunk *head;

void *ircd_alloc(size_t sz)
{
  struct chunk *c = calloc(1, sizeof(struct chunk) + sz);
  if (c == NULL)
    return NULL;

  if (head)
    head->prev = c;
  c->next = head;
  head = c;

  return c->memory;
}

void ircd_free(void *ptr)
{
  struct chunk *c;

  if (!ptr)
    return;

  c = (struct chunk *) (((char *) ptr) - sizeof(struct chunk));

  if (c->prev)
    c->prev->next = c->next;
  else
    head = c->next;
  if (c->next)
    c->next->prev = c->prev;

  free(c);
}

void *ircd_realloc(void *ptr, size_t sz)
{
  struct chunk *c;

  if (!ptr)
    return ircd_alloc(sz);

  if (!sz)
  {
    ircd_free(ptr);
    return NULL;
  }

  c = (struct chunk *) (((char *) ptr) - sizeof(struct chunk));

  c = realloc(c, sizeof(struct chunk) + sz);
  if (c == NULL)
    return NULL;

  if (c->prev)
    c->prev->next = c;
  else
    head = c;
  if (c->next)
    c->next->prev = c;

  return c->memory;
}

void ircd_free_all()
{
  struct chunk *c, *next;

  for (c = head; c; c = next)
  {
    next = c->next;
    free(c);
  }

  head = NULL;
}

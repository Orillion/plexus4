/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

extern int plexus_main(int, char*[]);

void
plexus_up(void)
{
  plexus_up_conf(RESOURCEDIR "/test.conf");
}

void
plexus_up_conf(const char *configfile)
{
  char pidfile[PATH_MAX];

  snprintf(pidfile, sizeof(pidfile), "%s.%d", PPATH, getpid());

  char *args[] = {
    SPATH,
    "-configfile",
    (char *) configfile,
    "-pidfile",
    pidfile,
    "-foreground",
    "-test",
    NULL /* C standard requires argv[argc] == NULL */
  };

  plexus_main(sizeof(args) / sizeof(*args) - 1, args);
}

void
plexus_down(void)
{
  plexus_join_all_and_exit();
  server_die("End of test", 0); // this will exit
}

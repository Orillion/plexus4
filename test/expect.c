/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

#define READBUF_SIZE 16384

static char readBuf[READBUF_SIZE];

void
expect(struct PlexusClient *client, const char *str)
{
  for (;;)
  {
    io_loop();

    for (;;)
    {
      int length = extract_one_line(&client->buf_recvq, readBuf);
      if (length == 0)
        break;

      if (match(str, readBuf))
      {
        tlog("WAITING FOR [%s]: discarding %s", str, readBuf);
        continue;
      }

      tlog("WAITING FOR [%s]: match %s", str, readBuf);
      return;
    }
  }
}

void
expect_numeric(struct PlexusClient *client, enum irc_numerics numeric)
{
  char buffer[IRCD_BUFSIZE];

  snprintf(buffer, sizeof(buffer), ":%s %03d %s *", me.name, numeric, client->name);

  expect(client, buffer);
}

void
expect_message(struct PlexusClient *client, struct Client *from, const char *message)
{
  char buffer[IRCD_BUFSIZE];

  if (from == NULL)
    snprintf(buffer, sizeof(buffer), "%s *", message);
  else if (IsClient(from))
    snprintf(buffer, sizeof(buffer), ":%s!%s@%s %s *", from->name, from->username, from->host, message);
  else
    snprintf(buffer, sizeof(buffer), ":%s %s *", ID_or_name(from, client->client), message);

  expect(client, buffer);
}

void
expect_message_args(struct PlexusClient *client, struct Client *from, const char *fmt, ...)
{
  char buffer[IRCD_BUFSIZE];
  va_list args;
  int off;

  if (IsClient(from))
    off = snprintf(buffer, sizeof(buffer), ":%s!%s@%s ", from->name, from->username, from->host);
  else
    off = snprintf(buffer, sizeof(buffer), ":%s ", from->name);

  va_start(args, fmt);
  vsnprintf(buffer + off, sizeof(buffer) - off, fmt, args);
  va_end(args);

  expect(client, buffer);
}

void
expect_pingwait(struct PlexusClient *client, struct Client *to)
{
  irc_ping(client, client->name, to->name);
  expect_message(client, to, "PONG");
}

void
expect_nickchange_to(struct PlexusClient *client, struct Client *from, const char *to)
{
  char buffer[IRCD_BUFSIZE];

  snprintf(buffer, sizeof(buffer), ":%s!%s@%s NICK :%s", from->name, from->username, from->host, to);

  expect(client, buffer);
}

void
expect_nickchange_from(struct PlexusClient *client, struct Client *to, const char *from)
{
  char buffer[IRCD_BUFSIZE];

  snprintf(buffer, sizeof(buffer), ":%s!%s@%s NICK :%s", from, to->username, to->host, to->name);

  expect(client, buffer);
}

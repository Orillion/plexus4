/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

struct irc_ssaddr
ip_parse(const char *ip)
{
  struct addrinfo hint, *res = NULL;
  union
  {
    struct irc_ssaddr ss;
    struct sockaddr_in in;
    struct sockaddr_in6 in6;
  } u;

  memset(&hint, 0, sizeof(hint));

  hint.ai_family = PF_UNSPEC;
  hint.ai_flags = AI_NUMERICHOST;

  ck_assert(!getaddrinfo(ip, NULL, &hint, &res));
  assert(res->ai_family == AF_INET || res->ai_family == AF_INET6);

  memset(&u, 0, sizeof(u));

  u.ss.ss.ss_family = res->ai_family;

  if (res->ai_family == AF_INET)
    inet_pton(res->ai_family, ip, &u.in.sin_addr);
  else
    inet_pton(res->ai_family, ip, &u.in6.sin6_addr);

  fix_irc_ssaddr(&u.ss);
  freeaddrinfo(res);

  return u.ss;
}


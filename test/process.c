/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

#define MAX_PROCS 32

static pid_t pids[MAX_PROCS];

static void
add_process(pid_t pid)
{
  for (int i = 0; i < MAX_PROCS; ++i)
  {
    if (!pids[i])
    {
      pids[i] = pid;
      return;
    }
  }

  // check will kill pid
  ck_abort_msg("out of process space");
}

static pid_t
fork_process()
{
  pid_t pid = check_fork();

  ck_assert(pid >= 0);

  if (pid > 0)
  {
    // parent.
    add_process(pid);
  }
  else
  {
    // child.
    memset(pids, 0, sizeof(pids)); // child has no subprocesses
  }

  return pid;
}

void
plexus_fork(void (*callback)())
{
  if (!plexus_branch())
  {
    callback();
    exit(EXIT_SUCCESS);
  }
}

bool
plexus_branch(void)
{
  pid_t pid = fork_process();

  return pid > 0;
}

/* lifted from check */
static int
waserror(int status, int signal_expected)
{
    int was_sig = WIFSIGNALED(status);
    int was_exit = WIFEXITED(status);
    int exit_status = WEXITSTATUS(status);
    int signal_received = WTERMSIG(status);

    return ((was_sig && (signal_received != signal_expected)) ||
            (was_exit && exit_status != 0));
}

void
plexus_join_all_and_exit(void)
{
  for (int i = 0; i < MAX_PROCS; ++i)
  {
    if (!pids[i])
      continue;

    int status;
    pid_t pid;

    do
    {
      pid = waitpid(pids[i], &status, 0);
    }
    while (pid == -1 && errno != ECHILD);

    if (waserror(status, 0))
    {
      ck_abort_msg("Error in plexus_join_all_and_exit(), pid %d: %s",
                   pid, strerror(errno));
    }
  }

  exit(EXIT_SUCCESS);
}

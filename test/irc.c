/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

void
irc_join(struct PlexusClient *client, const char *channel)
{
  io_write(client, "JOIN %s", channel);
}

void
irc_join_key(struct PlexusClient *client, const char *channel, const char *key)
{
  io_write(client, "JOIN %s %s", channel, key);
}

void
irc_part(struct PlexusClient *client, const char *channel)
{
  io_write(client, "PART %s :Leaving", channel);
}

void
irc_invite(struct PlexusClient *from, struct Channel *chptr, struct Client *to)
{
  io_write(from, "INVITE %s %s", to->name, chptr->chname);
}

void
irc_client_set_cmode(struct PlexusClient *client, struct Channel *chptr, const char *modes)
{
  io_write(client, "MODE %s %s", chptr->chname, modes);
}

void
irc_client_set_umode(struct PlexusClient *client, const char *modes)
{
  io_write(client, "MODE %s %s", client->client->name, modes);
}

void
irc_privmsg(struct PlexusClient *client, const char *target, const char *message)
{
  io_write(client, "PRIVMSG %s :%s", target, message);
}

void
irc_notice(struct PlexusClient *client, const char *target, const char *message)
{
  io_write(client, "NOTICE %s :%s", target, message);
}

void
irc_nick(struct PlexusClient *client, const char *newnick)
{
  io_write(client, "NICK %s", newnick);
}

void
irc_ping(struct PlexusClient *client, const char *origin, const char *destination)
{
  io_write(client, "PING %s %s", origin, destination);
}

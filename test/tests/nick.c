/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

START_TEST(nick)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  irc_nick(client1, "test3");
  expect_nickchange_to(client1, client1->client, "test3");
  strlcpy(client1->name, "test3", sizeof(client1->name));
  ck_assert_str_eq(client1->client->name, "test3");

  irc_join(client1, "#a");
  expect_message(client1, client1->client, "JOIN");

  irc_join(client2, "#a");
  expect_message(client2, client2->client, "JOIN");

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  irc_nick(client1, "test4");
  expect_nickchange_to(client1, client1->client, "test4");
  strlcpy(client1->name, "test4", sizeof(client1->name));
  ck_assert_str_eq(client1->client->name, "test4");

  expect_nickchange_from(client2, client1->client, "test3");
}
END_TEST

void
nick_setup(Suite *s)
{
  TCase *tc = tcase_create("nick");

  tcase_add_checked_fixture(tc, NULL, plexus_down);
  tcase_add_test(tc, nick);

  suite_add_tcase(s, tc);
}

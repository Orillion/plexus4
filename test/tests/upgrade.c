/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

#ifdef HAVE_LIBJANSSON

#include <jansson.h>
#include <setjmp.h>

extern json_t *plexus_upgrade;
extern jmp_buf plexus_upgrade_jmp;

static char tmp[] = "/tmp/plexus.upgrade.test.XXXXXX";

static void
other()
{
  plexus_up_conf(RESOURCEDIR "/test2.conf");

  WAIT_FOR(hash_find_client("plexus4.") != NULL);
}

static void
check_upgrade(void)
{
  struct Client *client = hash_find_client("test");
  ck_assert_ptr_ne(client, NULL);

  client = hash_find_client("plexus4.2");
  ck_assert_ptr_ne(client, NULL);

  ck_assert_ptr_ne(hash_find_channel("#a"), NULL);
}

static void
upgrade_target()
{
  /* target of the upgrade */
  struct stat statbuf;

  while (stat(tmp, &statbuf) || statbuf.st_size == 0)
  {
    // can't WAIT_FOR as no ircd exists yet
    usleep(1000);
  }

  FILE *f = fopen(tmp, "r");
  json_error_t err;
  json_t *json = json_loadf(f, 0, &err);
  fclose(f);

  ck_assert_ptr_ne(json, NULL);

  plexus_up();

  upgrade_unserialize(json, 1);
  json_decref(json);

  check_upgrade();
}

static void
prepare_server(void)
{
  // get server state into what we want to upgrade

  struct MaskItem *conf = find_matching_name_conf(CONF_SERVER, "plexus4.2", NULL, NULL, 0);
  ck_assert_ptr_ne(conf, NULL);

  serv_connect(conf, NULL);

  WAIT_FOR(hash_find_client("plexus4.2") != NULL);

  // abuse migrating sflags to prevent servers exiting from closing it
  struct Client *otherserver = hash_find_client("plexus4.2");
  AddSFlag(otherserver, SERVER_FLAGS_MIGRATING_QUEUE_WRITE);
  AddSFlag(otherserver, SERVER_FLAGS_MIGRATING_BLOCK_READ);

  struct PlexusClient *client = client_register("test");

  irc_join(client, "#a");
  expect_message(client, client->client, "JOIN");
}

START_TEST(test_upgrade)
{
  mkstemp(tmp);

  plexus_fork(other);

  plexus_fork(upgrade_target);

  plexus_up();

  prepare_server();

  if (!setjmp(plexus_upgrade_jmp))
    server_upgrade();

  FILE *f = fopen(tmp, "w");
  int ret = json_dumpf(plexus_upgrade, f, JSON_COMPACT | JSON_ENSURE_ASCII);
  fclose(f);

  ck_assert_int_eq(ret, 0);

  plexus_join_all_and_exit();
}
END_TEST

#endif // HAVE_LIBJANSSON

void
upgrade_setup(Suite *s)
{
  TCase *tc = tcase_create("upgrade");

#ifdef HAVE_LIBJANSSON
  tcase_add_test(tc, test_upgrade);
#endif

  suite_add_tcase(s, tc);
}

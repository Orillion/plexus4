/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

START_TEST(ping)
{
  struct PlexusClient *client1 = client_register("test1");

  irc_ping(client1, "Adam", me.id);
  expect_numeric(client1, ERR_NOSUCHSERVER);

  irc_ping(client1, "Adam", "");
  expect_message_args(client1, &me, "PONG %s :%s", me.name, "Adam");

  irc_ping(client1, "Adam", "Adam");
  expect_numeric(client1, ERR_NOSUCHSERVER);
}
END_TEST

void
ping_setup(Suite *s)
{
  TCase *tc = tcase_create("ping");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, ping);

  suite_add_tcase(s, tc);
}

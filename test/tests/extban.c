/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

/**
 * -- extended bans unit tests --
 *
 * client1 - Dummy client that simply idles #a with op and sets
 *           modes.
 * client2 - Account with the account name we want to test against
 * client3 - Dummy client with no account name that we use for join
 *           tests. (e.g. we can still join even with an account ban
 *           present) We could use client1, but then we need to
 *           worry about making the channel perminant so we don't
 *           lose the bans. Even if we did, we'd still need to get
 *           op again.
 */

static void
join_no_part(struct PlexusClient *const c)
{
  irc_join(c, "#a");
  expect_message(c, c->client, "JOIN");
  ck_assert_ptr_ne(hash_find_channel("#a"), NULL);
}

static void
join_cycle(struct PlexusClient *const c, const int numeric)
{
  irc_join(c, "#a");
  if (numeric)
  {
    if (numeric == ERR_LINKCHANNEL)
    {
      expect_message(c, c->client, "JOIN");
      irc_part(c, "#b");
      expect_message(c, c->client, "PART");
    }
    else
    {
      expect_numeric(c, numeric);
    }
  }
  else
  {
    expect_message(c, c->client, "JOIN");
    irc_part(c, "#a");
    expect_message(c, c->client, "PART");
  }
}

static void
cmode_expect(struct PlexusClient *const c, struct Channel *const chptr,
             const char *const modes, const int count)
{
  irc_client_set_cmode(c, chptr, modes);
  expect_message(c, c->client, "MODE");
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), count);
}

static void
set_account(struct PlexusClient *const c, const char *const account)
{
  strlcpy(c->client->suser, account, sizeof(c->client->suser));
}

static void
set_fingerprint(struct PlexusClient *const c, const char *const fingerprint)
{
  free(c->client->certfp);
  c->client->certfp = xstrdup(fingerprint);
}

static void
set_gecos(struct PlexusClient *const c, const char *const gecos)
{
  strlcpy(c->client->info, gecos, sizeof(c->client->info));
}

static void
check_set_can_flood(struct PlexusClient *const c)
{
  SetCanFlood(c->client);
  ck_assert(c->client->flags & FLAGS_CANFLOOD);
}

static void
test_chan_join(const char *const name, const int numeric,
          struct PlexusClient *const client1,
          struct PlexusClient *const client2,
          struct PlexusClient *const client3)
{
  char ban[IRCD_BUFSIZE];
  snprintf(ban, IRCD_BUFSIZE, "+b %s", name);
  struct Channel *chptr = hash_find_channel("#a");

  cmode_expect(client1, chptr, ban, 1);
  join_cycle(client2, numeric);
  join_cycle(client3, 0);

  memset(ban, 0, IRCD_BUFSIZE);
  snprintf(ban, IRCD_BUFSIZE, "-b %s", name);

  cmode_expect(client1, chptr, ban, 0);
  join_cycle(client2, 0);
  join_cycle(client3, 0);
}

static void
test_chan_talk(const char *const name, const int numeric,
          struct PlexusClient *const client1,
          struct PlexusClient *const client2,
          struct PlexusClient *const client3)
{
  char ban[IRCD_BUFSIZE];
  snprintf(ban, IRCD_BUFSIZE, "+b %s", name);
  struct Channel *chptr = hash_find_channel("#a");

  cmode_expect(client1, chptr, ban, 1);

  irc_privmsg(client2, "#a", "test");
  if (numeric)
    expect_numeric(client2, numeric);
  else
    expect_message(client1, client2->client, "PRIVMSG");

  irc_notice(client2, "#a", "test");
  if (!numeric)
    expect_message(client1, client2->client, "NOTICE");

  irc_privmsg(client1, "#a", "test");
  expect_message(client3, client1->client, "PRIVMSG");
  irc_notice(client3, "#a", "test");
  expect_message(client1, client3->client, "NOTICE");

  memset(ban, 0, IRCD_BUFSIZE);
  snprintf(ban, IRCD_BUFSIZE, "-b %s", name);

  cmode_expect(client1, chptr, ban, 0);

  irc_privmsg(client2, "#a", "test");
  expect_message(client1, client2->client, "PRIVMSG");
  irc_notice(client2, "#a", "test");
  expect_message(client1, client2->client, "NOTICE");

  irc_privmsg(client3, "#a", "test");
  expect_message(client1, client3->client, "PRIVMSG");
  irc_notice(client3, "#a", "test");
  expect_message(client1, client3->client, "NOTICE");
}

#define TEST_CHAN_JOINABILITY(name, num) (test_chan_join(name, num, client1, client2, client3))
#define TEST_CHAN_TALKABILITY(name, num) (test_chan_talk(name, num, client1, client2, client3))

START_TEST(account)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2"),
                      *client3 = client_register("test3");

  /* Throttling is not the point of this test */
  check_set_can_flood(client1);
  check_set_can_flood(client2);
  check_set_can_flood(client3);

  /* Set account name to badacct */
  set_account(client2, "badacct");

  join_no_part(client1);  /* Set "test1" as #a op */
  join_cycle(client2, 0); /* New channel; no bans, can join */
  join_cycle(client3, 0); /* New channel; no bans, can join */

  TEST_CHAN_JOINABILITY("a:badacct", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("a:bad*", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("a:*", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("a:some*thing/16", 0);
  TEST_CHAN_JOINABILITY("a:ug980fsdayuhg9dfguyadf9u", 0);
  TEST_CHAN_JOINABILITY("a:4324324343243", 0);
  TEST_CHAN_JOINABILITY("a:!!!!!!!", 0);
}
END_TEST

START_TEST(fingerprint)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2"),
                      *client3 = client_register("test3");

  /* Throttling is not the point of this test */
  check_set_can_flood(client1);
  check_set_can_flood(client2);
  check_set_can_flood(client3);

  /* Set account name to badacct */
  set_fingerprint(client2, "0759cd50ccdfa128cc07c4769dddd8ac362b393d");

  join_no_part(client1);  /* Set "test1" as #a op */
  join_cycle(client2, 0); /* New channel; no bans, can join */
  join_cycle(client3, 0); /* New channel; no bans, can join */

  TEST_CHAN_JOINABILITY("z:0759cd50ccdfa128cc07c4769dddd8ac362b393d", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("z:0759cd50ccdfa12?cc07c4769dddd8ac362b393d", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("z:?75?cd50?cdf?128c?07c?769?ddd?ac3?2b3?3d", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("z:*75*cd50*cdf*128c*0*c*769*ddd*ac3*2b3*3d", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("z:*fa128cc07*", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("z:*", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("z:some*thing/16", 0);
  TEST_CHAN_JOINABILITY("z:ug980fsdayuhg9dfguyadf9u", 0);
  TEST_CHAN_JOINABILITY("z:4324324343243", 0);
  TEST_CHAN_JOINABILITY("z:!!!!!!!", 0);
}
END_TEST

START_TEST(gecos)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2"),
                      *client3 = client_register("test3");

  /* Throttling is not the point of this test */
  check_set_can_flood(client1);
  check_set_can_flood(client2);
  check_set_can_flood(client3);

  /* Set account name to badacct */
  set_gecos(client2, "An Example IRC Client");

  join_no_part(client1);  /* Set "test1" as #a op */
  join_cycle(client2, 0); /* New channel; no bans, can join */
  join_cycle(client3, 0); /* New channel; no bans, can join */

  TEST_CHAN_JOINABILITY("r:An?Example?IRC?Client", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("r:An*Example*I?C*Client", ERR_BANNEDFROMCHAN);
  TEST_CHAN_JOINABILITY("r:*I*C*", ERR_BANNEDFROMCHAN);
  /* TEST_CHAN_JOINABILITY("r:*", ERR_BANNEDFROMCHAN); <-- Basically the equiv of +b *!*@* */
  TEST_CHAN_JOINABILITY("r:some*thing/16", 0);
  TEST_CHAN_JOINABILITY("r:ug980fsdayuhg9dfguyadf9u", 0);
  TEST_CHAN_JOINABILITY("r:4324324343243", 0);
  TEST_CHAN_JOINABILITY("r:!!!!!!!", 0);
}
END_TEST

START_TEST(redirect)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2"),
                      *client3 = client_register("test3");

  /* Throttling is not the point of this test */
  check_set_can_flood(client1);
  check_set_can_flood(client2);
  check_set_can_flood(client3);

  set_account(client2, "badacct");
  set_fingerprint(client2, "0759cd50ccdfa128cc07c4769dddd8ac362b393d");
  set_gecos(client2, "An Example IRC Client");

  join_no_part(client1);  /* Set "test1" as #a op */
  join_cycle(client2, 0); /* New channel; no bans, can join */
  join_cycle(client3, 0); /* New channel; no bans, can join */

  /* Join test1 in to #b, set ban exception redirect for #a */
  irc_join(client1, "#b");
  expect_message(client1, client1->client, "JOIN");
  struct Channel *chptr = hash_find_channel("#b");
  ck_assert_ptr_ne(chptr, NULL);
  irc_client_set_cmode(client1, chptr, "+e f:#a");
  expect_message(client1, client1->client, "MODE");

  /* Test join test1 and test2 to #b */
  irc_join(client2, "#b");
  expect_message(client2, client2->client, "JOIN");
  irc_part(client2, "#b");
  expect_message(client2, client2->client, "PART");
  irc_join(client3, "#b");
  expect_message(client3, client3->client, "JOIN");
  irc_part(client3, "#b");
  expect_message(client3, client3->client, "PART");

  /* account ban forward */
  TEST_CHAN_JOINABILITY("a:badacct#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("a:*#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("a:b?d*t#b", ERR_LINKCHANNEL);

  /* certfp ban forward */
  TEST_CHAN_JOINABILITY("z:0759cd50ccdfa128cc07c4769dddd8ac362b393d#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("z:*75*cd50*cdf*128c*0*c*769*ddd*ac3*2b3*3d#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("z:*fa128cc07*#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("z:*#b", ERR_LINKCHANNEL);

  /* geco ban forward */
  TEST_CHAN_JOINABILITY("r:An?Example?IRC?Client#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("r:An*Example*I?C*Client#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("r:*I*C*#b", ERR_LINKCHANNEL);

  /* Regular ban forward */
  TEST_CHAN_JOINABILITY("test2!*@*#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("*2!*@*#b", ERR_LINKCHANNEL);
  TEST_CHAN_JOINABILITY("t??t2!*@*#b", ERR_LINKCHANNEL);
}
END_TEST

START_TEST(mute)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2"),
                      *client3 = client_register("test3");

  /* Throttling is not the point of this test */
  check_set_can_flood(client1);
  check_set_can_flood(client2);
  check_set_can_flood(client3);

  join_no_part(client1);
  join_no_part(client2);
  join_no_part(client3);

  TEST_CHAN_TALKABILITY("m:test2!*@*", ERR_CANNOTSENDTOCHAN);
  TEST_CHAN_TALKABILITY("m:*2!*@*", ERR_CANNOTSENDTOCHAN);
  TEST_CHAN_TALKABILITY("m:t??t2!*@*", ERR_CANNOTSENDTOCHAN);
  TEST_CHAN_TALKABILITY("m:safddfsafd!*@*", 0);
}
END_TEST

#undef TEST_CHAN_TALKABILITY
#undef TEST_CHAN_JOINABILITY

void
extban_setup(Suite *s)
{
  TCase *tc = tcase_create("extban");

  tcase_add_checked_fixture(tc, NULL, plexus_down);
  tcase_add_test(tc, account);
  tcase_add_test(tc, fingerprint);
  tcase_add_test(tc, gecos);
  tcase_add_test(tc, redirect);
  tcase_add_test(tc, mute);

  suite_add_tcase(s, tc);
}
